export const shareQuestions = {
    methods: {
        shareYes() {
            this.$http
                .post(
                    `${this.$store.getters.endpoint}/questions/${this.$route.params.id}/yes`,
                    {
                    },
                    {
                        headers: {
                            Authorization: "Bearer " + localStorage.getItem("token")
                        }
                    }
                )
                .then(() => {
                    this.$router.push('/questions');
                });
        },
        shareNo() {
            this.$http
                .post(
                    `${this.$store.getters.endpoint}/questions/${this.$route.params.id}/no`,
                    {
                    },
                    {
                        headers: {
                            Authorization: "Bearer " + localStorage.getItem("token")
                        }
                    }
                )
                .then(() => {
                    // this.$router.push('/questions');

                });
        },
    }
}