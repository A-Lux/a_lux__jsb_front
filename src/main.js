import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import i18n from './i18n'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
// eslint-disable-next-line
import { isMobile, isAndroid, isIOS } from 'mobile-device-detect';
import axios from 'axios'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import moment from 'moment'
import VuePlyr from 'vue-plyr'
import GSignInButton from 'vue-google-signin-button'
import FBSignInButton from 'vue-facebook-signin-button'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);
Vue.use(FBSignInButton)
Vue.use(GSignInButton)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBWLVQMhWRJRj-awz5YunrRgPL20pHnWxo',
    libraries: 'places', 
  },
  installComponents: true
})
var SocialSharing = require('vue-social-sharing');
Vue.use(VuePlyr, {
  autopause: true
});
Vue.use(SocialSharing);

Vue.prototype.moment = moment

Vue.use(VueAwesomeSwiper)

Vue.prototype.$http = axios;

// VueAuthenticate.factory(Vue.prototype.$http, {
//   baseUrl: 'http://localhost:8080'
// })

Vue.use(VueRouter)

Vue.component('PointsSymbol', require('@/components/PointsSymbol.vue').default);

Vue.config.productionTip = false

let detect = {
  isMobile,
  isAndroid,
  isIOS
};

Vue.prototype.$detect = detect;

//eslint-disable-next-line
VK.init({
  apiId: 7209136
});

new Vue({
  vuetify,
  i18n,
  router,
  store,
  detect,
  render: h => h(App)
}).$mount('#app')
