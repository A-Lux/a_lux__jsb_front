import Vue from 'vue'
import Router from 'vue-router'
import LoginPageComponent from '@/components/Authorization/LoginPageComponent.vue'
import RegisterComponent from '@/components/Authorization/RegisterComponent.vue'
import MainPageComponent from '@/components/MainPageComponent.vue'
import CategoryPageComponent from '@/components/CategoryPageComponent.vue'
import CompetitionPageComponent from '@/components/Competition/CompetitionPageComponent.vue';
import Questions from '@/components/Question/List.vue';
import Success from '@/components/Question/Success.vue';
import Fail from '@/components/Question/Fail.vue';
import Question from '@/components/Question/CardFull.vue';
import Test from '@/components/Question/Test.vue';
import ContactsPageComponent from '@/components/ContactsPageComponent.vue';
import OfertsPageComponent from '@/components/OfertsPageComponent.vue'
import Rating from '@/components/RatingPage.vue';
import TermsPageComponent from '@/components/TermsPageComponent.vue'
import Profile from '@/components/Profile/index.vue';
import Reference from '@/components/Reference.vue';
import Details from '@/components/Profile/details.vue';
import History from '@/components/Profile/History.vue';
import Prize from '@/components/Profile/Prize.vue';
import Code from '@/components/Authorization/Code.vue';
import Forgot from '@/components/Authorization/Forgot.vue'
import WorkPage from '@/components/Work/WorkPage.vue'
import Edit from '@/components/Profile/Edit.vue';
import Rules from '@/components/Rules.vue'
import Meta from 'vue-meta'

Vue.use(Router)
Vue.use(Meta)


export default new Router({
    routes: [
        {
            path: '/',
            name: 'main',
            component: MainPageComponent,
        },
        {
            path: '/category/:id',
            name: 'category',
            component: CategoryPageComponent
        },
        {
            path: '/login',
            name: 'login',
            component: LoginPageComponent,
        },
        {
            path: '/register',
            name: 'register',
            component: RegisterComponent,
        },
        {
            path: '/forgot',
            name: 'forgot',
            component: Forgot,
        },
        {
            path: '/code',
            name: 'code',
            component: Code,
        },
        {
            path: '/competition/:id',
            name: 'competition',
            component: CompetitionPageComponent,
        },
        {
            path: '/work/:id',
            name: 'work',
            component: WorkPage,
        },
        {
            path: '/questions',
            name: 'questions',
            component: Questions
        },
        {
            path: '/questions/:id',
            name: 'question',
            component: Question
        },
        {
            path: '/questions/:id/test',
            name: 'test',
            component: Test
        },
        {
            path: '/questions/:id/win',
            name: 'win',
            component: Success
        },
        {
            path: '/questions/:id/lose',
            name: 'lose',
            component: Fail
        },
        {
            path: '/rating',
            name: 'rating',
            component: Rating
        },
        {
            path: '/contacts',
            name: 'contact',
            component: ContactsPageComponent
        },
        {
            path: '/oferts',
            name: 'oferta',
            component: OfertsPageComponent
        },
        {
            path: '/terms',
            name: 'terms',
            component: TermsPageComponent
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
        },
        {
            path: '/profile/details',
            name: 'details',
            component: Details
        },
        {
            path: '/profile/history',
            name: 'history',
            component: History
        },
        {
            path: '/profile/edit',
            name: 'edit',
            component: Edit
        },
        {
            path: '/profile/prize',
            name: 'prize',
            component: Prize
        },
        {
            path: '/profile/:id',
            name: 'profile',
            component: Profile,
        },
        {
            path: '/profile/:id/details',
            name: 'details',
            component: Details
        },
        {
            path: '/profile/:id/history',
            name: 'history',
            component: History
        },
        {
            path: '/reference',
            name: 'reference',
            component: Reference
        },
        {
            path: '/rules',
            name: 'rules',
            component: Rules
        }
    ]
})
