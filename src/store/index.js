import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    languages: [],
    keys: {},
    plyr: 0,
    endpoint: 'http://api.en.iprize.co/users',
    authpoint: 'http://api.en.iprize.co/auth',
    categories: []
  },
  mutations: {
    setLanguages(state, languages) {
      state.languages = languages
    },
    setKeys(state, keys) {
      state.keys = keys
    },
    setCategories(state, categories) {
      state.categories = categories
    },
    setPlyr(state, plyr) {
      state.plyr = plyr
    }
  },
  getters: {
    languages: state => state.languages,
    endpoint: state => state.endpoint,
    categories: state => state.categories,
    authpoint: state => state.authpoint,
    plyr: state => state.plyr,
    keys: state => state.keys
  },
  actions: {
  },
  modules: {
  }
})
